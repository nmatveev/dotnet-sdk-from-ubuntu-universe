FROM debian:bookworm-slim
LABEL Name=dotnet-sdk-from-ubuntu-universe Version=0.0.1
ARG DOTNET_SDK_VERSION="6.0"

RUN <<EOF
    set -e
    apt-get -y update && apt-get install -y gnupg
    # Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys "871920D1991BC93C"
    touch /etc/apt/sources.list
    echo "deb http://cz.archive.ubuntu.com/ubuntu jammy main" >> /etc/apt/sources.list
    echo "deb http://security.ubuntu.com/ubuntu jammy-security main universe" >> /etc/apt/sources.list

    apt-get -y update && apt-get install -y dotnet-sdk-${DOTNET_SDK_VERSION}
    apt clean
EOF

CMD ["sh", "-c", "dotnet --version"]
